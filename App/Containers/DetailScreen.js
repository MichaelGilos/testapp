import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ApplicationStyles, Metrics } from '../Themes'
import FastImage from 'react-native-fast-image'

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  }
})

const DetailScreen = ({ navigation }) => {
  const { item } = navigation.state.params

  const { artistName, trackName, country, releaseDate, artistViewUrl } = item

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>DETAILS</Text>
      <FastImage
          style={{ width: 100, height: 100 }}
          source={{
            uri: item.artworkUrl100,
            priority: FastImage.priority.normal
          }}
          resizeMode={FastImage.resizeMode.contain}
        />
      <Text>{artistName}</Text>
      <Text>{trackName}</Text>
      <Text>{country}</Text>
      <Text>{releaseDate}</Text>
    </View>
  )
}

export default DetailScreen
