import AsyncStorage from '@react-native-community/async-storage'
import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { FlatList, StyleSheet, Text, View } from 'react-native'
import FastImage from 'react-native-fast-image'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { ApplicationStyles, Metrics } from '../Themes'

const styles = StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16
  }
})

const MainScreen = ({ navigation }) => {
  const [selectedId, setSelectedId] = useState(null)

  const [items, setItems] = useState([])

  const loadPersistedItems = async () => {
    const loadedItems = await AsyncStorage.getItem('items').then((data) => {
      return (data) ? JSON.parse(data) : fetchData()
    })
    console.tron.log({ loadedItems })

    setItems(loadedItems)
  }

  const fetchData = () =>
    Axios.get(
      'https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all'
    ).then(({ data }) => {
      const withTrackname = data.results.filter((item) => item.trackName)
      // setItems(withTrackname)

      // persist items
      AsyncStorage.setItem('items', JSON.stringify(withTrackname))

      return withTrackname
    })

  useEffect(() => {
    // Axios.get(
    //   'https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all'
    // ).then(({ data }) => {
    //   const withTrackname = data.results.filter((item) => item.trackName)
    //   setItems(withTrackname)

    //   // persist items
    //   AsyncStorage.setItem('items', JSON.stringify(withTrackname))
    // })

    loadPersistedItems()
  }, [])

  const handleItemPress = (item) => {
    setSelectedId(item.trackId)

    navigation.navigate('DetailScreen', { item })
  }

  const renderItem = ({ item }) => {
    const backgroundColor = item.trackId === selectedId ? '#6e3b6e' : '#f9c2ff'

    return (
      <TouchableOpacity
        key={item.trackId}
        onPress={() => handleItemPress(item)}
        style={{
          padding: 20,
          marginVertical: 8,
          marginHorizontal: 16,
          backgroundColor
        }}>
        <Text>{item.trackName}</Text>
        <FastImage
          style={{ width: 50, height: 50 }}
          source={{
            uri: item.artworkUrl60,
            priority: FastImage.priority.normal
          }}
          resizeMode={FastImage.resizeMode.contain}
        />
        <Text>
          {item.currency} {item.trackPrice}
        </Text>
        <Text>{item.primaryGenreName}</Text>
      </TouchableOpacity>
    )
  }

  return (
    <View
      style={{
        flex: 1
      }}>
      <FlatList
        // contentContainerStyle={styles.container}
        data={items}
        renderItem={renderItem}
        keyExtractor={(item) => item.trackId}
        extraData={[selectedId, items]}
      />
    </View>
  )
}

export default MainScreen
